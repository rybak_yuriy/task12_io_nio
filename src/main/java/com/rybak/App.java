package com.rybak;

import com.rybak.view.MyView;

public class App {

    public static void main(String[] args) {
        new MyView().start();
    }
}
