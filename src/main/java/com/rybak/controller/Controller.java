package com.rybak.controller;

import com.rybak.model.task1.FirstTask;
import com.rybak.model.task1.Garden;
import com.rybak.model.task1.Tree;
import com.rybak.model.task2.SecondTask;
import com.rybak.model.task4.FourthTask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Controller {

    private static Logger log = LogManager.getLogger(Controller.class);

    FirstTask firstTask;
    SecondTask secondTask;
    FourthTask fourthTask;

    public Controller() {
        firstTask = new FirstTask(new File("src/main/resources/task1.txt"));
        secondTask = new SecondTask(new File("src/main/resources/task2.pdf"));
        fourthTask = new FourthTask(new File("src/main/resources/comments.txt"));
    }

    public void doFirstTask() {
        log.info("First task started");
        Tree apple1 = new Tree("apple", 2010);
        Tree apple2 = new Tree("apple", 2011);
        Tree cherry1 = new Tree("cherry", 2015);
        Tree pear1 = new Tree("pear", 2014);
        Garden garden = new Garden("BestGarden");
        List<Tree> trees = new ArrayList<>();
        trees.add(apple1);
        trees.add(apple2);
        trees.add(cherry1);
        trees.add(pear1);
        garden.setTrees(trees);
        log.info("Garden before serialization: " + garden.toString());
        firstTask.serializeObjects(garden);
        Garden deserializedGarden = (Garden) firstTask.deserializeObjects();
        log.info("Garden after deserialization: " + deserializedGarden.toString());
        log.info("First task test finished");

    }

    public void doSecondTask() {
        log.info("Second task started");
        long before = System.nanoTime();
        int bufferSize = 1 * 1024;
        int readedSize = secondTask.readWithBuffer(bufferSize);
        long after = System.nanoTime();
        long diff = after - before;
        log.info(String
                .format("Read with buffer: buffer size - %d bytes, file size - %,d bytes. Spent time: %,d ms",
                        bufferSize, readedSize, diff));

        before = System.nanoTime();
        bufferSize = 1 * 1024 * 1024;
        readedSize = secondTask.readWithBuffer(bufferSize);
        after = System.nanoTime();

        log.info(String
                .format("Read with buffer: buffer size - %,d bytes, file size - %,d bytes. Spent time: %,d ms",
                        bufferSize, readedSize, after - before));

        before = System.nanoTime();
        readedSize = secondTask.readWithoutBuffer();
        after = System.nanoTime();
        log.info(String
                .format("Read without buffer:  file size - %,d bytes. Spent time: %,d ms, " +
                                "what is %d times longer than wiht buffer size 1024 bytes ",
                        readedSize, after - before, (after - before) / diff));

    }

    public void doFourthTask() {
        log.info("Fourth task started");
        fourthTask.readComments()
                .forEach(comment -> log.info(comment));
        log.info("Fourth task finished.");
    }
}
