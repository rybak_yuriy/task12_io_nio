package com.rybak.model.task2;

import java.io.*;

public class SecondTask {

    File file;

    public SecondTask(File file) {
        this.file = file;
    }

    public int readWithoutBuffer() {
        int count = 0;
        try {
            InputStream inputStream = new FileInputStream(file);
            int data = inputStream.read();
            while (data != -1) {
                data = inputStream.read();
                count++;
            }
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

    public int readWithBuffer(int bufferSize) {
        int count = 0;
        try {
            InputStream inputStream = new FileInputStream(file);
            InputStream bufferedInputStream = new BufferedInputStream(inputStream,bufferSize);
            int data = inputStream.read();
            while (data != -1) {
                data = bufferedInputStream.read();
                count++;
            }
            bufferedInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }
}

