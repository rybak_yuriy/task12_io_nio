package com.rybak.model.task1;

import java.io.Serializable;

public class Tree implements Serializable {


    private String type;
    private int yearOfPlanting;


    public Tree(String type, int yearOfPlanting) {
        this.type = type;
        this.yearOfPlanting = yearOfPlanting;
    }

    @Override
    public String toString() {
        return "Tree{" +
                "type='" + type + '\'' +
                ", yearOfPlanting=" + yearOfPlanting +
                '}';
    }
}
