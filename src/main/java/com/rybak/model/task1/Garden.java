package com.rybak.model.task1;

import java.io.Serializable;
import java.util.List;

public class Garden implements Serializable {

    private transient String name;
    private List<Tree> trees;
    private transient int square;

    public Garden(String name, List<Tree> trees, int square) {
        this.name = name;
        this.trees = trees;
        this.square = square;
    }

    public Garden() {
    }

    public Garden(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Garden{" +
                "name='" + name + '\'' +
                ", trees=" + trees +
                ", square=" + square +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTrees(List<Tree> trees) {
        this.trees = trees;
    }

    public void setSquare(int square) {
        this.square = square;
    }
}
