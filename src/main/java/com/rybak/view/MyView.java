package com.rybak.view;

import com.rybak.controller.Controller;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    Scanner scanner = new Scanner(System.in);
    Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;

    public MyView() {
        controller = new Controller();
        menu = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();

        menu.put("1", "1 - Serialize and deserialize");
        menu.put("2", "2 - Read and write performance by usual and buffered reader");
        menu.put("3", "3 - ");
        menu.put("3", "4 - Read all comments from a file");
        menu.put("Q", "Q - Exit");

        menuMethods.put("1", this::doFirstTask);
        menuMethods.put("2", this::doSecondTask);
        menuMethods.put("4", this::doFourthTask);
    }

    private void doFourthTask() {
        controller.doFourthTask();
    }


    private void doFirstTask() {
        controller.doFirstTask();
    }

    private void doSecondTask() {
        controller.doSecondTask();
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    public void start() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println(" Q - quit");
            System.out.print("Please, select menu point: ");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                menuMethods.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
        scanner.close();
        System.exit(0);
    }
}
